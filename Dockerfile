FROM debian:8

MAINTAINER Seu Nome <seu_e-mail@provedor>

ENV DEBIAN_FRONTEND noninteractive

COPY sources.list /etc/apt/sources.list

RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN apt-get update && apt-get install -y python-software-properties software-properties-common postgresql-9.4 postgresql-client-9.4 postgresql-contrib-9.4 supervisor sudo gzip rsyslog
RUN apt-get clean autoclean && apt-get autoremove -y
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY postgresql.conf /etc/postgresql/9.4/main/postgresql.conf 
COPY pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf
COPY create.sql.gz /tmp/create.sql.gz
COPY rsyslog.conf /etc/rsyslog.conf

RUN echo "postgres ALL=(ALL) NOPASSWD:/usr/bin/supervisord, /bin/rm -rf /tmp/*" >> /etc/sudoers
RUN mkdir -p /var/log/supervisor 

USER postgres

RUN /etc/init.d/postgresql start && psql --command "CREATE USER zabbix WITH SUPERUSER PASSWORD '4linux';" && createdb -O zabbix zabbixdb && zcat /tmp/create.sql.gz | psql zabbixdb && psql zabbixdb --command "GRANT SELECT,UPDATE,DELETE,INSERT ON ALL TABLES IN SCHEMA public TO zabbix;"
RUN sudo rm -rf /tmp/*

ENV TERM xterm
EXPOSE 5432

CMD ["/usr/bin/sudo", "/usr/bin/supervisord"]
